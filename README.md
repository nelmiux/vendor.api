## Running the Application

I personally used IntelliJ, but you can import it to Eclipse or any other IDE to run the service, the tests and see the code.

## Running from an IDE

You can run a Spring Boot application from your IDE as a simple Java application. However, you first need to import your project. Import steps vary depending on your IDE and build system. Most IDEs can import Maven projects directly. For example, Eclipse users can select Import…​ → Existing Maven Projects from the File menu.

If you cannot directly import your project into your IDE, you may be able to generate IDE metadata by using a build plugin. Maven includes plugins for Eclipse and IDEA. Gradle offers plugins for various IDEs.

Source: [https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html]

## Dependencies

* Apache Maven
* Tomcat
* Spring Boot
* Data-rest
* MySQL
* H2 for UnitTesting
* JPA
* Hibernate
* ModelMapper
* Springfox with Swagger and validators
* JUnit
* AWS RDS, the MySql DB is hosted on AWS

## Documentation

After running the local base url on dev env is http://localhost:8080/, you can find the API documentation on http://localhost:8080/swagger-ui.html.

## DB

I deliver the DB with no data, you need to add the testing data as your convenience.