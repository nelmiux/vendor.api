package com.mpac.vendor.api.core;
import org.springframework.hateoas.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AbstractResource extends ResourceSupport {

    private final long id;
    private final String address_line_1;
    private final String address_line_2;
    private final String city;
    private final String state;
    private final String postal_code;
    private final String country;

    protected AbstractResource(AbstractEntity entity) {
        id = entity.getId();
        address_line_1 = entity.getAddressLine1();
        address_line_2 = entity.getAddressLine2();
        city = entity.getCity();
        state = entity.getState();
        postal_code = entity.getPostalCode();
        country = entity.getCountry();
    }

    @JsonProperty("id")
    public Long getResourceId() {
        return id;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public String getCountry() {
        return country;
    }
}