package com.mpac.vendor.api.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.Identifiable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"version", "createdAt", "updatedAt"}, allowGetters = true)
public abstract class AbstractEntity implements Identifiable<Long> {

	@Id()
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NotNull(message = "Address Line 1 field is required")
	@Size(max = 500)
	@Column(name = "address_line_1")
	private String addressLine1;

	@Size(max = 200)
	@Column(name = "address_line_2")
	private String addressLine2;

	@NotNull(message = "City field is required")
	@Size(max = 250)
	private String city;

	@NotNull(message = "State field is required")
	@Size(max = 250)
	private String state;

	@NotNull(message = "Postal Code field is required")
	@Size(max = 10)
	@Column(name = "postal_code")
	private String postalCode;

	@NotBlank(message = "Country field is required")
	@Size(max = 200)
	private String country;

	@Column(name = "created_at", nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt;

	@Column(name = "updated_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;

	// Getters and Setters
	@Override
	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("address_line_1")
	public String getAddressLine1() {
		return addressLine1;
	}

	@JsonProperty("address_line_1")
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@JsonProperty("address_line_2")
	public String getAddressLine2() {
		return addressLine2;
	}

	@JsonProperty("address_line_2")
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@JsonProperty("postal_code")
	public String getPostalCode() {
		return postalCode;
	}

	@JsonProperty("postal_code")
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
