package com.mpac.vendor.api.account.web;

import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.core.ResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class AccountResourceAssembler extends ResourceAssembler<Account, AccountResource> {

    private final EntityLinks entityLinks;

    private static final String UPDATE_REL = "update";
    private static final String CREATE_REL = "create";
    private static final String CONTACTS_REL = "contacts";
    private static final String CONTACTS_PATH = "/contacts";

    @Autowired
    public AccountResourceAssembler(EntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    @Override
    public AccountResource toResource(Account account) {

        AccountResource resource = new AccountResource(account);

        final Link selfLink = entityLinks.linkToSingleResource(account);

        final LinkBuilder assocLink = entityLinks.linkForSingleResource(account);

        final Link contactLink = assocLink.slash(CONTACTS_PATH).withSelfRel();

        resource.add(selfLink.withSelfRel());
        resource.add(selfLink.withRel(UPDATE_REL));
        resource.add(selfLink.withRel(CREATE_REL));
        resource.add(contactLink.withRel(CONTACTS_REL));

        return resource;
    }

}