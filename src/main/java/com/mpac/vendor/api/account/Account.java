package com.mpac.vendor.api.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mpac.vendor.api.core.AbstractEntity;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Account")
public class Account extends AbstractEntity implements Serializable {

    @NotNull(message = "Name field is a required")
    @Size(max = 250, message = "Name cannot be longer than 250 characters")
    private String companyName;

    // Getters and Setters
    @JsonProperty("company_name")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("company_name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
