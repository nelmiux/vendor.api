package com.mpac.vendor.api.account.web;
import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.contact.ContactRepository;
import com.mpac.vendor.api.contact.web.ContactResource;
import com.mpac.vendor.api.contact.web.ContactResourceAssembler;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

@RestController
@Api(tags = "Account/Contacts")
@RequestMapping("/accounts/{accountId}/")
@ExposesResourceFor(Account.class)
public class AccountContactController {

    private final ContactRepository repository;

    private final ContactResourceAssembler assembler;

    @Autowired
    public AccountContactController(ContactRepository repository, ContactResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @RequestMapping(value = "contacts", method = RequestMethod.GET)
    public ResponseEntity<Collection<ContactResource>> findContactsByAccountId(@PathVariable Long accountId) {
        Collection<Contact> contacts = repository.findByAccountId(accountId);
        return new ResponseEntity<>(assembler.toResourceCollection(contacts), HttpStatus.OK);
    }


    @RequestMapping(value = "contacts/{contactId}", method = RequestMethod.GET)
    public ResponseEntity findContactByAccountIdByContactId(@PathVariable Long accountId, @PathVariable Long contactId) {
        Optional<Contact> contact = repository.findByAccountId(accountId).stream().filter(x -> x.getId().equals(contactId)).findFirst();

        if (!contact.isPresent())
            throw new ResourceNotFoundException("account_id-" + accountId + ", contact_id-" + contactId);

        return new ResponseEntity<>(assembler.toResource(contact.get()), HttpStatus.OK);
    }
}
