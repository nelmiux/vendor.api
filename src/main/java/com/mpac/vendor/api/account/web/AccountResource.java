package com.mpac.vendor.api.account.web;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.core.AbstractResource;

public class AccountResource extends AbstractResource {

    private final String company_name;

    AccountResource(Account account) {
        super(account);
        company_name = account.getCompanyName();
    }

    public String getCompany_name() {
        return company_name;
    }
}