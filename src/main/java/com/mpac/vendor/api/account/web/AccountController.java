package com.mpac.vendor.api.account.web;
import com.mpac.vendor.api.exception.ResourceNotFoundException;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.account.AccountRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@Api(tags = "Accounts")
@RequestMapping("/accounts")
@ExposesResourceFor(Account.class)
public class AccountController {

    private final AccountRepository repository;

    private final AccountResourceAssembler assembler;

    @Autowired
    public AccountController(AccountRepository repository, AccountResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<AccountResource>> findAllAccounts() {
        Collection<Account> accounts = (List<Account>) repository.findAll();
        return new ResponseEntity<>(assembler.toResourceCollection(accounts), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<AccountResource> findAccountById(@PathVariable Long id) {
        Optional<Account> account = repository.findById(id);

        if (!account.isPresent())
            throw new ResourceNotFoundException("id-" + id);

        return new ResponseEntity<>(assembler.toResource(account.get()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<AccountResource> createAccount(@RequestBody Account account) {
        Account createdAccount = repository.save(account);
        return new ResponseEntity<>(assembler.toResource(createdAccount), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<AccountResource> updateAccount(@PathVariable Long id, @RequestBody Account updatedAccount) {
        Optional<Account> account = repository.findById(id);

        if (!account.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        updatedAccount.setId(id);
        repository.save(updatedAccount);

        return findAccountById(id);
    }
}
