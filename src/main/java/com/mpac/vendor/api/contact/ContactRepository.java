package com.mpac.vendor.api.contact;
import io.swagger.annotations.ApiParam;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "contacts", path = "contacts")
public interface ContactRepository extends PagingAndSortingRepository<Contact, Long> {

    List<Contact> findByAccountId(@Param("accountId") @ApiParam(value="ID of the account") Long accountId);
}
