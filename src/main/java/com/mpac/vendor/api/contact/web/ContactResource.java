package com.mpac.vendor.api.contact.web;
import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.core.AbstractResource;

public class ContactResource extends AbstractResource {

    private final String name;
    private final String email_address;

    ContactResource(Contact contact) {
        super(contact);
        name = contact.getName();
        email_address = contact.getEmailAddress();
    }

    public String getName() {
        return name;
    }

    public String getEmail_address() {
        return email_address;
    }
}