package com.mpac.vendor.api.contact.web;

import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.core.ResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ContactResourceAssembler extends ResourceAssembler<Contact, ContactResource> {

    private final EntityLinks entityLinks;

    private static final String UPDATE_REL = "update";
    private static final String CREATE_REL = "create";
    private static final String ACCOUNT_PATH = "/account";
    private static final String ACCOUNT_REL = "account";

    @Autowired
    public ContactResourceAssembler(EntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    @Override
    public ContactResource toResource(Contact contact) {

        ContactResource resource = new ContactResource(contact);

        final Link selfLink = entityLinks.linkToSingleResource(contact);

        final LinkBuilder assocLink = entityLinks.linkForSingleResource(contact);

        final Link accountLink = assocLink.slash(ACCOUNT_PATH).withSelfRel();

        resource.add(selfLink.withSelfRel());
        resource.add(selfLink.withRel(UPDATE_REL));
        resource.add(selfLink.withRel(CREATE_REL));
        resource.add(accountLink.withRel(ACCOUNT_REL));

        return resource;
    }
}