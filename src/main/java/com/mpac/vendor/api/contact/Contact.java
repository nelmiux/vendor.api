package com.mpac.vendor.api.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.core.AbstractEntity;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "Contact")
public class Contact extends AbstractEntity implements Serializable {

	@NotNull(message = "Name field is a required")
	@Size(max = 250, message = "Name cannot be longer than 250 characters")
	private String name;

	@NotNull(message = "Email Address field is required")
	@Size(max = 200, message = "Email Address cannot be longer than 200 characters")
	@Email(message = "Email address")
	private String emailAddress;

	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	// Getters and Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("email_address")
	public String getEmailAddress() {
		return emailAddress;
	}

	@JsonProperty("email_address")
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
