package com.mpac.vendor.api.contact.web;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.exception.ResourceNotFoundException;
import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.contact.ContactRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@Api(tags = "Contacts")
@RequestMapping("/contacts")
@ExposesResourceFor(Contact.class)
public class ContactController {

	private final ContactRepository repository;

	private final ContactResourceAssembler assembler;

	@Autowired
	public ContactController(ContactRepository repository, ContactResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<ContactResource>> findAllContacts() {
		List<Contact> contacts = (List<Contact>) repository.findAll();
		return new ResponseEntity<>(assembler.toResourceCollection(contacts), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ContactResource> createContact(@RequestBody Resource<Contact> contactResource) {
		Contact createdContact = repository.save(contactResource.getContent());
		return new ResponseEntity<>(assembler.toResource(createdContact), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ContactResource> findContactById(@PathVariable Long id) {
		Optional<Contact> contact = repository.findById(id);

		if (!contact.isPresent())
			throw new ResourceNotFoundException("id-" + id);

        return new ResponseEntity<>(assembler.toResource(contact.get()), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<ContactResource> updateContact(@PathVariable Long id, @RequestBody Resource<Contact> updatedResourseContact) {
		Optional<Contact> contact = repository.findById(id);

		if (!contact.isPresent())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		Contact updatedContact = updatedResourseContact.getContent();

		updatedContact.setId(id);
		repository.save(updatedContact);

		return findContactById(id);
	}
}
