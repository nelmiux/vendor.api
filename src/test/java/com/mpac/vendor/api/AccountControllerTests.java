package com.mpac.vendor.api;
import com.mpac.vendor.api.account.Account;
import org.junit.Test;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AccountControllerTests extends ApplicationTests {

    private final String endPoint = "accounts";

    @Test
    public void findAllAccounts() throws Exception {

        Address address1 = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        Address address2 = new Address("9876 ABC Drive", "apt 7B", "San Francisco", "CA", "94016", "US");
        String company1 = "Company Close 1";
        String company2 = "Company FarAway 2";

        accountRepository.save(sampleAccount(company1, address1));
        accountRepository.save(sampleAccount(company2, address2));

        mockMvc.perform(get("/" + endPoint)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());
    }

    @Test
    public void findAccountById() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String company = "Company Close 1";

        Account account = accountRepository.save(sampleAccount(company, address));

        mockMvc.perform(get("/" + endPoint + "/" + account.getId())
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.company_name").value(company))
                .andExpect(jsonPath("$.address_line_1").value(address.getAddressLine1()))
                .andExpect(jsonPath("$.address_line_2").value(address.getAddressLine2()))
                .andExpect(jsonPath("$.city").value(address.getCity()))
                .andExpect(jsonPath("$.state").value(address.getState()))
                .andExpect(jsonPath("$.postal_code").value(address.getPostalCode()))
                .andExpect(jsonPath("$.country").value(address.getCountry()))
                .andDo(print());
    }

    @Test
    public void findAccountById404() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String company = "Company Close 1";

        Account account = accountRepository.save(sampleAccount(company, address));

        mockMvc.perform(get("/" + endPoint + "/1111")
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void createAccount() throws Exception {
        Address address = new Address("9876 ABC Drive", "apt 7B", "San Francisco", "CA", "94016", "US");
        String company = "Company FarAway";

        Account account = sampleAccount(company, address);

        mockMvc.perform(post("/" + endPoint + "/")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(account))
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.company_name").value(company))
                .andExpect(jsonPath("$.address_line_1").value(address.getAddressLine1()))
                .andExpect(jsonPath("$.address_line_2").value(address.getAddressLine2()))
                .andExpect(jsonPath("$.city").value(address.getCity()))
                .andExpect(jsonPath("$.state").value(address.getState()))
                .andExpect(jsonPath("$.postal_code").value(address.getPostalCode()))
                .andExpect(jsonPath("$.country").value(address.getCountry()))
                .andDo(print());
    }

    @Test
    public void updateAccount() throws Exception {
        Address address = new Address("5468 Bld", null, "NY", "NY", "10001", "US");
        String company = "Old Company";

        Account account = accountRepository.save(sampleAccount(company, address));

        account.setCompanyName("New Company");
        account.setAddressLine2("bld 777");

        mockMvc.perform(put("/" + endPoint + "/" + account.getId())
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(account))
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.company_name").value(account.getCompanyName()))
                .andExpect(jsonPath("$.address_line_2").value(account.getAddressLine2()))
                .andDo(print());
    }

    @Test
    public void updateAccount404() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String company = "Company Close 1";

        Account account = accountRepository.save(sampleAccount(company, address));

        mockMvc.perform(put("/" + endPoint + "/1111")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(account)))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

}