package com.mpac.vendor.api;

import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.contact.ContactRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class AccountContactControllerTests extends ApplicationTests {

    private final String endPoint = "accounts";

    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void findContactsByAccountId() throws Exception {
        Account account = saveSampleAccount();

        Contact contact = saveSampleContact(account);

        mockMvc.perform(get("/" + endPoint + "/" + account.getId() + "/contacts")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(contact.getName()))
                .andDo(print());
    }

    @Test
    public void findContactByAccountIdByContactId() throws Exception {
        Account account = saveSampleAccount();

        Contact contact = saveSampleContact(account);

        mockMvc.perform(get("/" + endPoint + "/" + account.getId() + "/contacts/" + contact.getId())
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(contact.getName()))
                .andDo(print());
    }

    private Contact saveSampleContact(Account account) {
        Address address = new Address("5468 Bld", null, "NY", "NY", "10001", "US");
        String name = "Old Name";
        String email = "xddv@devxcvf.com";

        Contact contact = sampleContact(name, email, address);
        contact.setAccount(account);

        return contactRepository.save(contact);
    }

    private Account saveSampleAccount() {
        Address address = new Address("14A ABC Drive", null, "Austin", "TX", "78705", "US");
        String company = "Big Company";

        return accountRepository.save(sampleAccount(company, address));
    }
}