package com.mpac.vendor.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.account.AccountRepository;
import com.mpac.vendor.api.contact.Contact;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Profile("test")
@WebAppConfiguration
@Transactional
public abstract class ApplicationTests {

    final String devBaseUrl = "http://localhost:8080/";

    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected WebApplicationContext appContext;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(appContext).build();
    }

    protected Account sampleAccount(String companyName, Address address) {
        ModelMapper modelMapper = new ModelMapper();
        Account account = modelMapper.map(address, Account.class);
        account.setCompanyName(companyName);
        return account;
    }

    protected Contact sampleContact(String name, String email, Address address) {
        ModelMapper modelMapper = new ModelMapper();
        Contact contact = modelMapper.map(address, Contact.class);
        contact.setName(name);
        contact.setEmailAddress(email);
        return contact;
    }
}
