package com.mpac.vendor.api;

import com.mpac.vendor.api.account.Account;
import com.mpac.vendor.api.contact.Contact;
import com.mpac.vendor.api.contact.ContactRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ContactControllerTests extends ApplicationTests {

    private final String endPoint = "contacts";

    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void findAllContacts() throws Exception {

        Address address1 = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        Address address2 = new Address("9876 ABC Drive", "apt 7B", "San Francisco", "CA", "94016", "US");
        String name1 = "Name 1";
        String email1 = "abc@def.com";
        String name2 = "Name 2";
        String email2 = "mnbvc@asdf.com";

        contactRepository.save(sampleContact(name1, email1, address1));
        contactRepository.save(sampleContact(name2, email2, address2));

        mockMvc.perform(get("/" + endPoint)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());
    }

    @Test
    public void findContactById() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String name = "Name 1";
        String email = "abc@def.com";

        Contact contact = contactRepository.save(sampleContact(name, email, address));

        mockMvc.perform(get("/" + endPoint + "/" + contact.getId())
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.email_address").value(email))
                .andExpect(jsonPath("$.address_line_1").value(address.getAddressLine1()))
                .andExpect(jsonPath("$.address_line_2").value(address.getAddressLine2()))
                .andExpect(jsonPath("$.city").value(address.getCity()))
                .andExpect(jsonPath("$.state").value(address.getState()))
                .andExpect(jsonPath("$.postal_code").value(address.getPostalCode()))
                .andExpect(jsonPath("$.country").value(address.getCountry()))
                .andDo(print());
    }

    @Test
    public void findContactById404() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String name = "Name 1";
        String email = "abc@def.com";

        Contact contact = contactRepository.save(sampleContact(name, email, address));

        mockMvc.perform(get("/" + endPoint + "/1111")
                .accept(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void createContact() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String name = "Name 1";
        String email = "abc@def.com";

        Contact contact = sampleContact(name, email, address);

        mockMvc.perform(post("/" + endPoint + "/")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(contact))
                .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.email_address").value(email))
                .andExpect(jsonPath("$.address_line_1").value(address.getAddressLine1()))
                .andExpect(jsonPath("$.address_line_2").value(address.getAddressLine2()))
                .andExpect(jsonPath("$.city").value(address.getCity()))
                .andExpect(jsonPath("$.state").value(address.getState()))
                .andExpect(jsonPath("$.postal_code").value(address.getPostalCode()))
                .andExpect(jsonPath("$.country").value(address.getCountry()))
                .andDo(print());
    }


    @Test
    public void updateContact() throws Exception {
        Address address = new Address("5468 Bld", null, "NY", "NY", "10001", "US");
        String name = "Old Name";
        String email = "xddv@devxcvf.com";

        Contact contact = contactRepository.save(sampleContact(name, email, address));

        contact.setName("New Name");
        contact.setAddressLine2("bld 777");

        mockMvc.perform(put("/" + endPoint + "/" + contact.getId())
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(contact))
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(contact.getName()))
                .andExpect(jsonPath("$.address_line_2").value(contact.getAddressLine2()))
                .andDo(print());
    }

    @Test
    public void updateContactWithAccount() throws Exception {
        Address addressAcc = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String company = "Company Test";

        Account account = accountRepository.save(sampleAccount(company, addressAcc));
        String accountUri = devBaseUrl + "accounts/" + account.getId();

        Address address = new Address("5468 Bld", null, "NY", "NY", "10001", "US");
        String name = "Old Name";
        String email = "xddv@devxcvf.com";

        Contact contact = contactRepository.save(sampleContact(name, email, address));

        mockMvc.perform(put("/" + endPoint + "/" + contact.getId())
                .contentType(APPLICATION_JSON).content("{\"account\": \"" + accountUri + "\"}")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void updateContact404() throws Exception {
        Address address = new Address("123 XYZ Street", null, "Austin", "TX", "78701", "US");
        String name = "Old Name";
        String email = "xddv@devxcvf.com";

        Contact contact = contactRepository.save(sampleContact(name, email, address));

        mockMvc.perform(put("/" + endPoint + "/1111")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(contact)))
                .andExpect(status().isNotFound())
                .andDo(print());
    }
}